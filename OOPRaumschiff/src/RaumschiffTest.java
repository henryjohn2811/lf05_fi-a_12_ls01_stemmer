
public class RaumschiffTest {

	public static void main(String[] args) {

		// Raumschiffe initialisieren

		Raumschiff klingonen = new Raumschiff(1, 100, 100, 100, 200, 2, "IKS Heghta");
		Ladung l1 = new Ladung("Ferengi Schneckensaft", 200);
		Ladung l2 = new Ladung("Batleth Klingonen Schwert", 200);
		klingonen.addLadung(l1);
		klingonen.addLadung(l2);

		Raumschiff romulaner = new Raumschiff(2, 80, 100, 100, 100, 2, "IRW Khazara");
		Ladung l3 = new Ladung("Borg-Schrott", 5);
		Ladung l4 = new Ladung("Rote Materie", 2);
		Ladung l5 = new Ladung("Plasma-Waffe", 50);
		romulaner.addLadung(l3);
		romulaner.addLadung(l4);
		romulaner.addLadung(l5);

		Raumschiff vulkanier = new Raumschiff(2, 100, 70, 100, 100, 2, "NiVar");
		Ladung l6 = new Ladung("Forschungssonde", 35);
		Ladung l7 = new Ladung("Photonentorpedo", 3);
		vulkanier.addLadung(l6);
		vulkanier.addLadung(l7);

// Auszufühurende Methoden

		klingonen.photonentorpedoSchiessen(romulaner);

		romulaner.phaserkanoneSchiessen(klingonen);

		vulkanier.nachrichtAnAlle("Gewalt ist nicht logisch.");

		klingonen.zustandRaumschiff();
		klingonen.ladungsverzeichnisAusgeben();

		vulkanier.reparaturDurchfuehren(true, true, true, vulkanier.getAndroidenAnzahl());

		vulkanier.photonentorpedosLaden(3);
		vulkanier.ladungsverzeichnisAufraeumen();

		klingonen.photonentorpedoSchiessen(romulaner);
		klingonen.photonentorpedoSchiessen(romulaner);

		klingonen.zustandRaumschiff();
		klingonen.ladungsverzeichnisAusgeben();
		romulaner.zustandRaumschiff();
		romulaner.ladungsverzeichnisAusgeben();
		vulkanier.zustandRaumschiff();
		vulkanier.ladungsverzeichnisAusgeben();

		System.out.println(Raumschiff.eintraegeLogbuchZurueckgeben());

	}

}
