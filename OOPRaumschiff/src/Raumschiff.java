import java.util.*;

public class Raumschiff {
// Attribute
	private int photonentorpedoAnzahl;
	private int energieversorungInProzent;
	private int schildeInProzent;
	private int huelleInProzent;
	private int lebenserhaltungssytemeInProzent;
	private int androidenAnzahl;
	private String schiffsname;
	private static ArrayList<String> broadcastKommunikator = new ArrayList<String>();
	private ArrayList<Ladung> ladungsverzeichnis = new ArrayList<Ladung>();

// Konstruktoren
	public Raumschiff() {

	}

	public Raumschiff(int photentorpedoAnzahl, int energieversorungInProzent, int schildeInProzent, int huelleInProzent,
			int lebenserhaltungssytemeInProzent, int androidenAnzahl, String schiffsname) {
		this.photonentorpedoAnzahl = photentorpedoAnzahl;
		this.energieversorungInProzent = energieversorungInProzent;
		this.schildeInProzent = schildeInProzent;
		this.huelleInProzent = huelleInProzent;
		this.lebenserhaltungssytemeInProzent = lebenserhaltungssytemeInProzent;
		this.androidenAnzahl = androidenAnzahl;
		this.schiffsname = schiffsname;
	}

// Getter und Setter
	public int getPhotentorpedoAnzahl() {
		return photonentorpedoAnzahl;
	}

	public void setPhotentorpedoAnzahl(int photentorpedoAnzahl) {
		this.photonentorpedoAnzahl = photentorpedoAnzahl;
	}

	public int getEnergieversorungInProzent() {
		return energieversorungInProzent;
	}

	public void setEnergieversorungInProzent(int energieversorungInProzent) {
		this.energieversorungInProzent = energieversorungInProzent;
	}

	public int getSchildeInProzent() {
		return schildeInProzent;
	}

	public void setSchildeInProzent(int schildeInProzent) {
		this.schildeInProzent = schildeInProzent;
	}

	public int getHuelleInProzent() {
		return huelleInProzent;
	}

	public void setHuelleInProzent(int huelleInProzent) {
		this.huelleInProzent = huelleInProzent;
	}

	public int getLebenserhaltungssytemeInProzent() {
		return lebenserhaltungssytemeInProzent;
	}

	public void setLebenserhaltungssytemeInProzent(int lebenserhaltungssytemeInProzent) {
		this.lebenserhaltungssytemeInProzent = lebenserhaltungssytemeInProzent;
	}

	public int getAndroidenAnzahl() {
		return androidenAnzahl;
	}

	public void setAndroidenAnzahl(int androidenAnzahl) {
		this.androidenAnzahl = androidenAnzahl;
	}

	public String getSchiffsname() {
		return schiffsname;
	}

	public void setSchiffsname(String schiffsname) {
		this.schiffsname = schiffsname;
	}

	public ArrayList<Ladung> getLadungsverzeichnis() {
		return ladungsverzeichnis;
	}

	public void setLadungsverzeichnis(ArrayList<Ladung> ladungsverzeichnis) {
		this.ladungsverzeichnis = ladungsverzeichnis;
	}

	public ArrayList<String> getBroadcastNachricht() {
		return broadcastKommunikator;
	}

	public void setBroadcastNachricht(ArrayList<String> broadcastNachricht) {
		Raumschiff.broadcastKommunikator = broadcastNachricht;
	}

	// Andere Methoden
	public void addLadung(Ladung ladung) {
		this.ladungsverzeichnis.add(ladung);
	}

	public void photonentorpedoSchiessen(Raumschiff ziel) {
		if (this.photonentorpedoAnzahl > 0) {
//			Scanner zielErfassung = new Scanner(System.in);
//
//			System.out.println("Bitte Ziel angeben:");
//			zielRaumschiff = zielErfassung.nextLine();

			setPhotentorpedoAnzahl(photonentorpedoAnzahl - 1);
			Raumschiff.broadcastKommunikator.add(this.schiffsname + ": " + "Photonentorpedo abgeschossen!");
			//System.out.println("Photonentorpedo abgeschossen!");
			System.out.println();
			this.treffer(ziel);

		} else {
			Raumschiff.broadcastKommunikator.add(this.schiffsname + ": " + "-=*Click*=-");
			Raumschiff.broadcastKommunikator.add(this.schiffsname + ": " + "Keine Photonentoredos vorhanden!");
			//System.out.println("-=*Click*=-");
			//System.out.println("Keine Photonentoredos vorhanden!");
			System.out.println();
		}
	}

	public void phaserkanoneSchiessen(Raumschiff ziel) {
		if (this.energieversorungInProzent >= 50) {
//			System.out.println("Bitte Ziel angeben:");
//			zielRaumschiff = zielErfassung.nextLine();
//
//			if (zielErfassung == null ) {
//				setEnergieversorungInProzent(energieversorungInProzent - 50);
//				System.out.println("Phaserkanone abgeschossen!");
//				System.out.println();
//			}
//			if (zielRaumschiff == this.Schiffsname) {
//				System.out.println("Sie k�nnen sich nicht selber beschie�en!");
//			} else {
//				System.out.println("Bitte ein g�ltiges Ziel angeben!");
//			}
			setEnergieversorungInProzent(energieversorungInProzent - 50);
			Raumschiff.broadcastKommunikator.add(this.schiffsname + ": " + "Phaserkanone abgeschossen!");
			//System.out.println("Phaserkanone abgeschossen!");
			System.out.println();
			this.treffer(ziel);

		} else {
			Raumschiff.broadcastKommunikator.add(this.schiffsname + ": " + "-=*Click*=-");
			Raumschiff.broadcastKommunikator.add(this.schiffsname + ": " + "Nicht genug Energie vorhanden!");
			//System.out.println("-=*Click*=-");
			//System.out.println("Nicht genug Energie vorhanden!");
			System.out.println();
		}
		;
	}

	private void treffer(Raumschiff ziel) {

		System.out.println(ziel.schiffsname + " wurde getroffen!");
		System.out.println();

		ziel.schildeInProzent = ziel.schildeInProzent - 50;

		if (ziel.schildeInProzent <= 0) {
			ziel.huelleInProzent = ziel.huelleInProzent - 50;

			if (ziel.huelleInProzent <= 0) {
				ziel.energieversorungInProzent = ziel.energieversorungInProzent - 50;
				Raumschiff.broadcastKommunikator.add(this.schiffsname + ": " + "Die Lebenserhaltungssysteme von " + ziel.schiffsname + " wurden zert�rt!");
				//System.out.println("Die Lebenserhaltungssysteme von " + ziel.schiffsname + " wurden zert�rt!");
				System.out.println();
			}

		}

	}

	public void nachrichtAnAlle(String nachricht) {
		System.out.println("-------------------------------------");
		System.out.println("Broadcast Message:");
		System.out.println(nachricht);
		System.out.println("-------------------------------------");
		System.out.println();
		Raumschiff.broadcastKommunikator.add(this.schiffsname + ": " + nachricht);
	}

	public static ArrayList<String> eintraegeLogbuchZurueckgeben() {
		return broadcastKommunikator;
	}

	public void photonentorpedosLaden(int anzahlTorpedos) {
		// Temp Variablen
		String ladungsname = " ";
		int anzahl = 0;
		boolean torpedosVorhanden = false;

		// Durchsuchen des Ladungsverzeichnis nach Photonentorpedos
		for (Ladung i : ladungsverzeichnis) {
			ladungsname = i.getBezeichnung();
			anzahl = i.getMenge();

			if (ladungsname.equals("Photonentorpedo")) {
				torpedosVorhanden = true;

				if (anzahlTorpedos >= anzahl) {
					System.out.println(anzahl + " Photonentorpedo(s) wurde(n) nachgeladen.");
					System.out.println();
					this.photonentorpedoAnzahl = (this.getPhotentorpedoAnzahl() + anzahl);
					i.setMenge(0);
				}

				else {
					System.out.println(anzahlTorpedos + " Photonentorpedo(s) wurde(n) nachgeladen.");
					this.photonentorpedoAnzahl = (this.getPhotentorpedoAnzahl() + anzahl);
					System.out.println();
					i.setMenge(anzahl - anzahlTorpedos);
				}
			}
		}
		if (torpedosVorhanden == false) {
			System.out.println();
			Raumschiff.broadcastKommunikator.add(this.schiffsname + ": " + "-=*Click*=-");
			//System.out.println("-=*Click*=-");
			System.out.println("Keine Photonentorpedos im Lager gefunden!");
			System.out.println();
		}
	}

	public void reparaturDurchfuehren(boolean schutzschilde, boolean energieversorgung, boolean schiffshuelle,
			int androidenAnzahl) {

		// Temp Variabeln
		int reparaturBedarf = 0;

		if (energieversorgung = true) {
			reparaturBedarf = reparaturBedarf + 1;
		}

		if (schutzschilde = true) {
			reparaturBedarf = reparaturBedarf + 1;
		}

		if (schiffshuelle = true) {
			reparaturBedarf = reparaturBedarf + 1;
		}

		// Zufallszahl
		Random zufall = new Random();
		int zufallsZahl = zufall.nextInt(100) + 1;

		// Reparatur
		if (androidenAnzahl > this.androidenAnzahl) {
			androidenAnzahl = this.androidenAnzahl;

		}

		if (reparaturBedarf > 0) {
			int reparatur = (zufallsZahl * androidenAnzahl) / reparaturBedarf;

			if (energieversorgung = true) {
				this.energieversorungInProzent = this.energieversorungInProzent + reparatur;
			}
			if (schutzschilde = true) {
				this.schildeInProzent = this.schildeInProzent + reparatur;
			}

			if (schiffshuelle) {
				this.huelleInProzent = this.huelleInProzent + reparatur;
			}
		}

	}

	public void zustandRaumschiff() {
		System.out.println(
				"�bersicht �ber den Zustand der Systeme vom Raumschiff " + this.schiffsname + " wird ausgegeben:");
		System.out.println("Zustand der H�lle: " + this.huelleInProzent + "%");
		System.out.println("Zustand der Schilde: " + this.schildeInProzent + "%");
		System.out.println("Zustand der Energieversorgung: " + this.energieversorungInProzent + "%");
		System.out.println("Zustand der Lebenserhaltungssysteme: " + this.lebenserhaltungssytemeInProzent + "%");
		System.out.println();

	}

	public void ladungsverzeichnisAusgeben() {
		System.out.println(this.ladungsverzeichnis);
		System.out.println();

	}

	public void ladungsverzeichnisAufraeumen() {
		for (int j = 0; j < this.getLadungsverzeichnis().size(); j++) {
			if (this.getLadungsverzeichnis().get(j).getMenge() == 0) {
				this.getLadungsverzeichnis().remove(j);
			}
		}
		System.out.println("Ladungsverzeichnis von " + this.schiffsname + " wurde aufger�umt.");
		System.out.println();
	}
}
