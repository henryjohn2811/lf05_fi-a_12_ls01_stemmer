/**
  *   Aufgabe:  Recherechieren Sie im Internet !
  * 
  *   Sie d�rfen nicht die Namen der Variablen ver�ndern !!!
  *
  *   Vergessen Sie nicht den richtigen Datentyp !!
  *
  *
  * @version 1.0 from 06.10.2021
  * @author Henry Stemmer
  */

public class WeltDerZahlen {

  public static void main(String[] args) {
    
    /*  *********************************************************
    
         Zuerst werden die Variablen mit den Werten festgelegt!
    
    *********************************************************** */
    // Im Internet gefunden ?
    // Die Anzahl der Planeten in unserem Sonnesystem                    
    byte anzahlPlaneten = 8;
    
    // Anzahl der Sterne in unserer Milchstra�e
    long anzahlSterne = 400_000_000_000l;
    
    // Wie viele Einwohner hat Berlin?
    int bewohnerBerlin = 3_766_082;
    
    // Wie alt bist du?  Wie viele Tage sind das?
    int alterTage = 6_887;
    
    // Wie viel wiegt das schwerste Tier der Welt?
    // Schreiben Sie das Gewicht in Kilogramm auf!
    int gewichtKilogramm = 200_000;  
    
    // Schreiben Sie auf, wie viele km� das gr��te Land er Erde hat?
    int flaecheGroessteLand = 17_098_242;
    
    // Wie gro� ist das kleinste Land der Erde?
    float flaecheKleinsteLand = 0.44f;  
    
    
    /*  *********************************************************
    
         Programmieren Sie jetzt die Ausgaben der Werte in den Variablen
    
    *********************************************************** */
    
    System.out.println("Anzhahl der Planeten: " + anzahlPlaneten);
    
    System.out.println("Anzahl der Sterne: " + anzahlSterne);
    
    System.out.println("Anzahl der Einwohner in Berlin: " + bewohnerBerlin);
    
    System.out.println("Mein Alter in Tagen: " + alterTage);
    
    System.out.println("Gewicht vom schwersten Tier der Welt in kg: " + gewichtKilogramm);
    
    System.out.println("Fl�che vom gr��ten Land der Welt in km�: " + flaecheGroessteLand);
    
    System.out.println("Fl�che vom kleinsten Land der Welt in km�: " + flaecheKleinsteLand);
    
    System.out.println(" *******  Ende des Programms  ******* ");
    
  }
}
