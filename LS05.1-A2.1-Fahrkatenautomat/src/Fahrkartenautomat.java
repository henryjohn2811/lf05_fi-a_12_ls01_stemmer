﻿import java.util.*;

class Fahrkartenautomat {
	public static void main(String[] args) {

		double zuZahlenderBetrag = fahrkartenbestellungErfassen();

		double rückgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag);

		fahrkartenAusgeben();

		rueckgeldAusgeben(rückgabebetrag);

//       Scanner tastatur = new Scanner(System.in);
//       
//       
//       // Variablen
//       // -----------
//       float zuZahlenderBetrag; 
//       float eingezahlterGesamtbetrag;
//       float eingeworfeneMünze;
//       float rückgabebetrag;
//       byte anzahlTickets; 
//       
//       
//       // Ticketeingabe       
//       // -----------
//       System.out.print("Ticketpreis (EURO): ");
//       zuZahlenderBetrag = tastatur.nextFloat();
//       
//       System.out.print("Anzahl der Tickets: ");
//       anzahlTickets = tastatur.nextByte();
//       
//       
//       // Preisberechnung       
//       // -----------  
//       zuZahlenderBetrag = anzahlTickets * zuZahlenderBetrag;
//      
//
//       // Geldeinwurf
//       // -----------
//       eingezahlterGesamtbetrag = 0.00f;
//       while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
//       {
//
//   		   System.out.printf("%s %.2f %s %n", "Noch zu zahlen:", (zuZahlenderBetrag - eingezahlterGesamtbetrag), "EURO");
//    	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
//    	   eingeworfeneMünze = tastatur.nextFloat();
//           eingezahlterGesamtbetrag += eingeworfeneMünze;
//       }
//
//       tastatur.close();
//       
//       // Fahrscheinausgabe
//       // -----------------
//       System.out.println("\nFahrschein wird ausgegeben");
//       for (int i = 0; i < 8; i++)
//       {
//          System.out.print("=");
//          try {
//			Thread.sleep(250);
//		} catch (InterruptedException e) {

//			e.printStackTrace();
//		}
//       }
//       System.out.println("\n\n");
//
//       
//       // Rückgeldberechnung und -Ausgabe
//       // -------------------------------
//       rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
//       if(rückgabebetrag > 0.0)
//       {
//    	   System.out.printf("%s %.2f %s %n", "Der Rückgabebetrag in Höhe von", rückgabebetrag , "EURO");
//    	   System.out.println("wird in folgenden Münzen ausgezahlt:");
//
//           while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
//           {
//        	  System.out.println("2 EURO");
//	          rückgabebetrag -= 2.0;
//           }
//           while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
//           {
//        	  System.out.println("1 EURO");
//	          rückgabebetrag -= 1.0;
//           }
//           while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
//           {
//        	  System.out.println("50 CENT");
//	          rückgabebetrag -= 0.5;
//           }
//           while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
//           {
//        	  System.out.println("20 CENT");
// 	          rückgabebetrag -= 0.2;
//           }
//           while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
//           {
//        	  System.out.println("10 CENT");
//	          rückgabebetrag -= 0.1;
//           }
//           while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
//           {
//        	  System.out.println("5 CENT");
// 	          rückgabebetrag -= 0.05;
//           }
//       }
//
//       System.out.println("\nVergessen Sie nicht, den Fahrschein "+
//                          "vor Fahrtantritt entwerten zu lassen!\n"+
//                          "Wir wünschen Ihnen eine gute Fahrt.");
	}

//    public static double fahrkartenbestellungErfassen() {
//    	Scanner tastatur = new Scanner(System.in);
//    	
//    	double zuZahlenderBetrag; 
//    	byte anzahlTickets; 
//        
//        System.out.print("Ticketpreis (EURO): ");
//        zuZahlenderBetrag = tastatur.nextDouble();
//        
//        System.out.print("Anzahl der Tickets: ");
//        anzahlTickets = tastatur.nextByte();
//        
//        zuZahlenderBetrag = anzahlTickets * zuZahlenderBetrag;
//              
//        return zuZahlenderBetrag;
//    	   	
//    	}

	public static double fahrkartenbestellungErfassen() {
		Scanner tastatur = new Scanner(System.in);

		double zuZahlenderBetrag = 0;
		double ticketPreis = 0;
		double tempBetrag = 0;
		byte anzahlTickets = 0;
		byte wahlTicket = 0;

		while (wahlTicket != 9) {
			System.out.println("Bitte wählen Sie ein Ticket aus:");
			System.out.println("1. Einzelfahrkarte AB");
			System.out.println("2. Kurzstrecke");
			System.out.println("3. Tageskarte ABC");
			System.out.println("4. Wochenkarte BC");
			System.out.println("9. Bezahlen");
			wahlTicket = tastatur.nextByte();

			while ((wahlTicket <= 0 || wahlTicket > 4) && wahlTicket != 9) {
				System.out.println("Bitte geben sie eine gültigte Ziffer ein!");
				wahlTicket = tastatur.nextByte();
			}

			if (wahlTicket == 1) {
				ticketPreis = 3;
			}

			else if (wahlTicket == 2) {
				ticketPreis = 2;
			}

			else if (wahlTicket == 3) {
				ticketPreis = 10;
			}

			else if (wahlTicket == 4) {
				ticketPreis = 37;
			}

			if (wahlTicket != 9) {
				System.out.println("Bitte geben sie die Anzahl der Tickets ein: ");
				anzahlTickets = tastatur.nextByte();
			}
			tempBetrag = ticketPreis * anzahlTickets;
			System.out.println("Aktueller Beitrag: " + tempBetrag + "€");
		}

		zuZahlenderBetrag = tempBetrag;
		return zuZahlenderBetrag;
	}

	public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
		Scanner tastatur = new Scanner(System.in);

		double eingezahlterGesamtbetrag;
		double eingeworfeneMünze;
		double rückgabebetrag;

		eingezahlterGesamtbetrag = 0.00d;

		while (eingezahlterGesamtbetrag < zuZahlenderBetrag) {

			System.out.printf("%s %.2f %s %n", "Noch zu zahlen:", (zuZahlenderBetrag - eingezahlterGesamtbetrag),
					"EURO");
			System.out.print("Eingabe (mind. 1Ct, höchstens 10 Euro): ");
			eingeworfeneMünze = tastatur.nextDouble();
			eingezahlterGesamtbetrag += eingeworfeneMünze;

		}

		tastatur.close();

		rückgabebetrag = (double) (eingezahlterGesamtbetrag - zuZahlenderBetrag);
		return rückgabebetrag;
	}

	public static void fahrkartenAusgeben() {
		System.out.println("\nFahrschein wird ausgegeben");
		for (int i = 0; i < 5; i++) {
			System.out.print("=");
			try {
				Thread.sleep(250);
			} catch (InterruptedException e) {

				e.printStackTrace();
			}
		}
		System.out.println("\n\n");
	}

	public static void rueckgeldAusgeben(double rückgabebetrag) {

		if (rückgabebetrag > 0.0d) {
			System.out.printf("%s %.2f %s %n", "Der Rückgabebetrag in Höhe von", rückgabebetrag, "EURO");
			System.out.println("wird in folgenden Münzen und Scheinen ausgezahlt:");

			while (rückgabebetrag >= 10.0d) // 10 EURO-Schein
			{
				System.out.println("10 EURO-Schein");
				rückgabebetrag -= 10.0d;
			}
			while (rückgabebetrag >= 5.0d) // 5 EURO-Schein
			{
				System.out.println("5 EURO-Schein");
				rückgabebetrag -= 5.0d;
			}
			while (rückgabebetrag >= 2.0d) // 2 EURO-Münzen
			{
				System.out.println("2 EURO");
				rückgabebetrag -= 2.0d;
			}
			while (rückgabebetrag >= 1.0d) // 1 EURO-Münzen
			{
				System.out.println("1 EURO");
				rückgabebetrag -= 1.0d;
			}
			while (rückgabebetrag >= 0.5d) // 50 CENT-Münzen
			{
				System.out.println("50 CENT");
				rückgabebetrag -= 0.5d;
			}
			while (rückgabebetrag >= 0.2d) // 20 CENT-Münzen
			{
				System.out.println("20 CENT");
				rückgabebetrag -= 0.2d;
			}
			while (rückgabebetrag >= 0.1d) // 10 CENT-Münzen
			{
				System.out.println("10 CENT");
				rückgabebetrag -= 0.1d;
			}
			while (rückgabebetrag >= 0.05d)// 5 CENT-Münzen
			{
				System.out.println("5 CENT");
				rückgabebetrag -= 0.05;
			}
			while (rückgabebetrag >= 0.02d)// 2 CENT-Münzen
			{
				System.out.println("2 CENT");
				rückgabebetrag -= 0.02d;
			}
			while (rückgabebetrag >= 0.01d)// 1 CENT-Münzen
			{
				System.out.println("1 CENT");
				rückgabebetrag -= 0.01d;
			}
		}

		System.out.println("\nVergessen Sie nicht, den Fahrschein " + "vor Fahrtantritt entwerten zu lassen!\n"
				+ "Wir wünschen Ihnen eine gute Fahrt.");

	}

}

// Aufgabe 5

//float zuZahlenderBetrag:
//Ich habe mich für "float" entschieden, da ich eine Variable benötige die für Kommazahlen geeignet ist, aber nicht mehr als 6 Dezimalstellen Genauigkeit.

//float eingezahlterGesamtbetrag:
//Ich habe mich für "float" entschieden, da ich eine Variable benötige die für Kommazahlen geeignet ist, aber nicht mehr als 6 Dezimalstellen Genauigkeit.

//float eingeworfeneMünze:
//Ich habe mich für "float" entschieden, da ich eine Variable benötige die für Kommazahlen geeignet ist, aber nicht mehr als 6 Dezimalstellen Genauigkeit.

//float rückgabebetrag:
//Ich habe mich für "float" entschieden, da ich eine Variable benötige die für Kommazahlen geeignet ist, aber nicht mehr als 6 Dezimalstellen Genauigkeit.

//byte anzahlTickets:
//Ich habe mich für "byte" entschieden, da ich es für unrealistisch halte, dass jemand mehr as 127 Tickets kaufen möchte.

//Aufgabe 6

//Bei "anzahl * einzelpreis" handelt es sich um einen multiplikativen Ausdruck. 
//Es wird das Produkt von "anzahl" und "einzelpreis" gebildet.